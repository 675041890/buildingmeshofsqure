using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MousePositionUtils 
{
    public static Vector3 MouseToPlanePosition()
    {
        Vector3 position = Vector3.zero;
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray,out RaycastHit info,10000,LayerMask.GetMask("Default")))
        {
            position = info.point;
        }
        return position;
    }


    
}
