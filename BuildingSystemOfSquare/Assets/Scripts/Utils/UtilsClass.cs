using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UtilsClass
{
    public const int sortingOrderDefault = 5000;

    /// <summary>
    /// 创建网格显示Text
    /// </summary>
    /// <param name="text">网格坐标</param>
    /// <param name="parent">父物体</param>
    /// <param name="localPosition">网格局部坐标</param>
    /// <param name="fontSize">字体大小</param>
    /// <param name="color">颜色</param>
    /// <param name="textAnchor">text锚点</param>
    /// <param name="textAlignment">text对齐方式</param>
    /// <param name="sortingOrder">text显示队列号</param>
    /// <returns></returns>
   public static TextMesh CreateWorldText(string text,Transform parent = null,Vector3 localPosition = default(Vector3),int fontSize = 40,Color? color = null,TextAnchor textAnchor = TextAnchor.UpperLeft,
                                    TextAlignment textAlignment = TextAlignment.Left,int  sortingOrder = sortingOrderDefault)
    {
        if (color == null)
            color = Color.white;
        return CreateWorldText(parent,text,localPosition,fontSize,(Color)color,textAnchor,textAlignment,sortingOrder);
        
    }

    public static TextMesh CreateWorldText(Transform parent,string text,Vector3 localPosition,int fontSize,Color color,TextAnchor textAnchor,TextAlignment textAlignment,int sortingOrder)
    {
        GameObject gameObject = new GameObject("World_Text",typeof(TextMesh));
        Transform transform = gameObject.transform;
        transform.SetParent(parent,false);
        transform.localPosition = localPosition;
        TextMesh textMesh = gameObject.GetComponent<TextMesh>();
        textMesh.anchor = textAnchor;
        textMesh.alignment = textAlignment;
        textMesh.text = text;
        textMesh.fontSize = fontSize;
        textMesh.color = color;
        textMesh.GetComponent<MeshRenderer>().sortingOrder = sortingOrder;
        return textMesh;
    }
}
