using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid 
{
    private int width;
    private int height;
    
    private float cellSize;
    private int[,] gridArray;

    private TextMesh[,] debugTextArray;
    public Grid(Transform transform, int width,int height,float cellSize)
    {
        this.width = width;
        this.height = height;
        this.cellSize = cellSize;

        gridArray = new int[width,height];
        debugTextArray = new TextMesh[width,height];

        for (int i = 0; i < gridArray.GetLength(0); i++)
        {
            for (int j = 0; j < gridArray.GetLength(1); j++)
            {
                debugTextArray[i,j] = CreateText(transform,gridArray[i,j].ToString(),new Vector3(i * cellSize + cellSize * .5f,0,j * cellSize + cellSize * .5f),20,Color.white, TextAnchor.MiddleCenter,TextAlignment.Center, 1);
                Debug.DrawLine(GetWorldPosition(i,j),GetWorldPosition(i,j+1),Color.white,100f);
                Debug.DrawLine(GetWorldPosition(i,j),GetWorldPosition(i + 1,j),Color.white,100f);
            }
        }

        Debug.DrawLine(GetWorldPosition(0, height), GetWorldPosition(width, height), Color.white, 100f);
        Debug.DrawLine(GetWorldPosition(width, 0), GetWorldPosition(width, height), Color.white, 100f);

        SetValue(2,1,56);
    }

    private Vector3 GetWorldPosition(int x, int z)
    {
        return new Vector3(x, 0, z) * cellSize;
    }

    private void GetXZ(Vector3 worldPosition,out int x,out int z)
    {
        x = Mathf.FloorToInt(worldPosition.x / cellSize);
        z = Mathf.FloorToInt(worldPosition.z / cellSize);
    }

    private TextMesh CreateText(Transform parent, string text, Vector3 localPos,int fontSize,Color color,TextAnchor anchor,TextAlignment alignment,int sortingOrder)
    {
        GameObject go = new GameObject("World_Text",typeof(TextMesh));
        Transform transform = go.transform;
        transform.SetParent(parent,false);
        transform.localPosition = localPos;
        TextMesh textMesh = go.GetComponent<TextMesh>();
        textMesh.anchor = anchor;
        textMesh.alignment = alignment;
        textMesh.text = text;
        textMesh.fontSize = fontSize;
        textMesh.color = color;
        textMesh.GetComponent<MeshRenderer>().sortingOrder = sortingOrder;
        return textMesh;
    }

    private void CreateCollider(Transform parent)
    {
        GameObject go = new GameObject("Collider", typeof(MeshCollider));
        Mesh mesh = new Mesh();
        Vector3[] vector3s = new Vector3[6]
                {
                    new Vector3(),
                    Vector3.zero,
                    Vector3.zero,
                    Vector3.zero,
                    Vector3.zero,
                    Vector3.zero,
                };
        mesh.vertices = vector3s;
    }

    private void SetValue(int x,int z,int value)
    {
        if(x >= 0 && z >= 0 && x < width && z < height)
        {
            gridArray[x, z] = value;
            debugTextArray[x, z].text = gridArray[x, z].ToString();
        }
    }

    public void SetValue(Vector3 worldPosition,int value)
    {
        int x, z;
        GetXZ(worldPosition,out x,out z);
        Debug.Log(x + "," + z );
        SetValue(x,z,value);
    }
}
