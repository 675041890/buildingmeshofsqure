using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridObject 
{
    public int x;
    public int z;
    public string PlaceableObjectName;
    public PlaceableObjectSO.Dir Direction;
    private GridXZ<GridObject> grid;

    private PlaceableObject placeableObject;

    public PlaceableObject PlaceableObject
    {
        get
        {
            return placeableObject;
        }
        set
        {
            placeableObject = value;
            grid.TriggerGridObjectChanged(x,z);
        }
    }

    public bool CanBuild { get { return placeableObject == null; } }

    public GridObject(GridXZ<GridObject> grid,int x,int z)
    {
        this.grid = grid;
        this.x = x;
        this.z = z;
    }

    public override string ToString()
    {
        return $"{x},{z}";
    }

    public void ClearPlaceableObject()
    {
        placeableObject = null;
        PlaceableObjectName = null;
        Direction = PlaceableObjectSO.Dir.Down;
        grid.TriggerGridObjectChanged(x, z);
    }
}
