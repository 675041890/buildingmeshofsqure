using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridBuildingSystem : Singleton<GridBuildingSystem>,IDisposable
{
    public PlaceableObjectSO PlaceableObjectSO;

    public int rowCount = 10;

    public int columnCount = 10;

    public int cellSize = 5;

    public Vector3 StartOrigin = Vector3.zero;

    public bool ShowDebug = false;

    public Action OnSelectChanged;
    public Action<bool> OnBuildingModeChanged;

    public bool IsBuildingMode { get; set; } = false;

    private GridXZ<GridObject> grid;
    private PlaceableObjectSO.Dir dir = PlaceableObjectSO.Dir.Down;

    public override void Awake()
    {
        base.Awake();

        grid = new GridXZ<GridObject>(rowCount,
            columnCount,
            cellSize,
            StartOrigin,
            (GridXZ<GridObject> g, int x, int z) => new GridObject(g, x, z),
            ShowDebug);

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            IsBuildingMode = !IsBuildingMode;
            //OnBuildingModeChanged.Invoke(IsBuildingMode);
        }

        if (IsBuildingMode)
        {
            if (Input.GetMouseButtonDown(0))
            {
                var mousePosition = MousePositionUtils.MouseToPlanePosition();
                grid.GetXZ(mousePosition,out int x,out int z);
                PlaceBuilding(PlaceableObjectSO,x,z,dir);
            }
        }
    }

    private void PlaceBuilding(PlaceableObjectSO placeableObjectSO,int x,int z,PlaceableObjectSO.Dir direction)
    {
        var gridPosList = placeableObjectSO.GetGridPositionList(new Vector2Int(x,z),dir);
        var canBuild = true;
        foreach (var gridPos in gridPosList)
        {
            if (!grid.GetGridObject(gridPos.x,gridPos.y).CanBuild)
            {
                canBuild = false;
                break;
            }
        }
        if (canBuild)
        {
            var rotOffset = placeableObjectSO.GetRotationOffset(dir);
            var placedWorldPos = grid.GetWorldPosition(x, z) + new Vector3(rotOffset.x, 0, rotOffset.y) * grid.CellSize;

            var placeObject = PlaceableObject.Create(
                placedWorldPos,
                new Vector2Int(x, z),
                dir,
                placeableObjectSO);

            foreach (var gridPos in gridPosList)
            {
                var gridToBuild = grid.GetGridObject(gridPos.x,gridPos.y);
                gridToBuild.PlaceableObject = placeObject;
                gridToBuild.PlaceableObjectName = placeObject.placeableObjectSO.NameString;
                gridToBuild.Direction = placeObject.dir;
            }
        }
    }

    public void Dispose()
    {
        throw new NotImplementedException();
    }
}
