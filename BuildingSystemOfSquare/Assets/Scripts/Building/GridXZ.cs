using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridXZ<TGridObject>
{
    public class OnGridObjectChangedEventArgs : EventArgs
    {
        public int x;
        public int z;
    }

    public event EventHandler<OnGridObjectChangedEventArgs> OnGridObjectChanged;

    public TGridObject[,] GridArray;
    public int Width;
    public int Height;
    public float CellSize;

    [JsonIgnore]
    public TextMesh[,] DebugTextArray { get; private set; }

    private Vector3 originPosition;

    public GridXZ() { }

    /// <summary>
    /// 设置网格
    /// </summary>
    /// <param name="width"></param>
    /// <param name="height"></param>
    /// <param name="cellSize"></param>
    /// <param name="originPosition"></param>
    /// <param name="createGridObject"></param>
    /// <param name="showDebug"></param>
    public GridXZ(int width,int height,float cellSize,Vector3 originPosition,Func<GridXZ<TGridObject>,int,int,TGridObject> createGridObject,bool showDebug)
    {
        this.Width = width;
        this.Height = height;
        this.CellSize = cellSize;
        this.originPosition = originPosition;

        GridArray = new TGridObject[width,height];

        for (int x = 0; x < GridArray.GetLength(0); x++)
        {
            for (int z = 0; z < GridArray.GetLength(1); z++)
            {
                GridArray[x, z] = createGridObject(this,x,z);
            }
        }

        if (showDebug)
        {
            //显示网格
            InitializeDebugTextArray(width,height,cellSize);
        }
    }

    private void InitializeDebugTextArray(int width, int height, float cellSize)
    {
        DebugTextArray = new TextMesh[width,height];
        for (int x = 0; x < GridArray.GetLength(0); x++)
        {
            for (int z = 0; z < GridArray.GetLength(1); z++)
            {
                //显示网格UI
                DebugTextArray[x, z] = UtilsClass.CreateWorldText(GridArray[x,z]?.ToString(),null,GetWorldPosition(x,z) + new Vector3(cellSize,0,cellSize) * .5f,15,Color.white,TextAnchor.MiddleCenter,TextAlignment.Center);
                //网格画线
                Debug.DrawLine(GetWorldPosition(x, z), GetWorldPosition(x, z + 1), Color.white, 100f);
                Debug.DrawLine(GetWorldPosition(x, z), GetWorldPosition(x + 1, z), Color.white, 100f);
            }
        }

        Debug.DrawLine(GetWorldPosition(0, height), GetWorldPosition(width, height), Color.white, 100f);
        Debug.DrawLine(GetWorldPosition(width,0), GetWorldPosition(width, height), Color.white, 100f);

        OnGridObjectChanged += (object sender, OnGridObjectChangedEventArgs eventArgs) =>
        {
            DebugTextArray[eventArgs.x, eventArgs.z].text = GridArray[eventArgs.x, eventArgs.z]?.ToString();
        };
    }

    public Vector3 GetWorldPosition(int x,int z)
    {
        return new Vector3(x, 0, z) * CellSize + originPosition;
    }

    public void GetXZ(Vector3 worldPosition,out int x,out int z)
    {
        x = Mathf.FloorToInt((worldPosition - originPosition).x / CellSize);
        z = Mathf.FloorToInt((worldPosition - originPosition).z / CellSize);
    }

    public void SetGridObject(int x,int z,TGridObject value)
    {
        if (x >= 0 && z >= 0 && x < Width && z < Height)
        {
            GridArray[x, z] = value;
            TriggerGridObjectChanged(x, z);

        }
    }

    public void TriggerGridObjectChanged(int x,int z)
    {
        OnGridObjectChanged?.Invoke(this,new OnGridObjectChangedEventArgs { x = x,z = z});
    }

    public void SetGridObject(Vector3 worldPosition,TGridObject value)
    {
        GetXZ(worldPosition,out int x,out int z);
        SetGridObject(x,z,value);
    }

    public TGridObject GetGridObject(int x,int z)
    {
        if (x >= 0 && z >= 0 && x < Width && z < Height)
        {
            return GridArray[x, z];
        }
        else
        {
            return default(TGridObject);
        }
    }

    public TGridObject GetGridObject(Vector3 worldPosition)
    {
        int x, z;
        GetXZ(worldPosition,out x,out z);
        return GetGridObject(x,z);
    }

    public Vector2Int ValidateGridPosition(Vector2Int gridPosition)
    {
        return new Vector2Int(Mathf.Clamp(gridPosition.x,0,Width - 1),Mathf.Clamp(gridPosition.y,0,Height - 1));
    }
}
