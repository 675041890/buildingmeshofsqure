using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu()]
public class PlaceableObjectSO : ScriptableObject
{
    public enum Dir
    {
        Down,
        Left,
        Up,
        Right
    }

    public string NameString;
    public Transform Prefab;
    public int Width;
    public int Height;

    public static Dir GetNextDir(Dir dir)
    {
        switch (dir)
        {
            default:
            case Dir.Down: return Dir.Left;
            case Dir.Left: return Dir.Up;
            case Dir.Up: return Dir.Right;
            case Dir.Right: return Dir.Down;
        }
    }

    /// <summary>
    /// 旋转角度
    /// </summary>
    /// <param name="dir"></param>
    /// <returns></returns>
    public int GetRotationAngle(Dir dir)
    {
        switch (dir)
        {
            default:
            case Dir.Down:
                return 0;
            case Dir.Left:
                return 90;
            case Dir.Up:
                return 180;
            case Dir.Right:
                return 270;
        }
    }

    /// <summary>
    /// 旋转后，物体朝向偏移
    /// </summary>
    /// <param name="dir"></param>
    /// <returns></returns>
    public Vector2Int GetRotationOffset(Dir dir)
    {
        switch (dir)
        {
            default:
            case Dir.Down: return new Vector2Int(0,0);
            case Dir.Left: return new Vector2Int(0, Width);
            case Dir.Up: return new Vector2Int(0, Width);
            case Dir.Right: return new Vector2Int(Height,0);

        }
    }

    /// <summary>
    /// 获取物体占用的所有网格位置
    /// </summary>
    /// <param name="offset"></param>
    /// <param name="dir"></param>
    /// <returns></returns>
    public List<Vector2Int> GetGridPositionList(Vector2Int offset,Dir dir)
    {
        List<Vector2Int> gridPositionList = new List<Vector2Int>();
        switch (dir)
        {
            default:
            case Dir.Down:
            case Dir.Up:
                for (int x = 0; x < Width; x++)
                {
                    for (int y = 0; y < Height; y++)
                    {
                        gridPositionList.Add(offset + new Vector2Int(x,y));
                    }
                }
                break;
            case Dir.Left:        
            case Dir.Right:
                for (int x = 0; x < Height; x++)
                {
                    for (int y = 0; y < Width; y++)
                    {
                        gridPositionList.Add(offset + new Vector2Int(x,y));
                    }
                }
                break;
        }
        return gridPositionList;
    }
}
